﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	
	public float patrolSpeed = 3.2f;
	public float chaseWalkSpeed = 5.5f;
	public float chaseRunSpeed = 8.7f;
	public float stoppingDistancePlayer = 1.5f;
	public float stoppingDistanceOther = 0f;
	public float secondsToWaitOnLost = 3f;
	public float healthPercentDamage = 100f;
	public float fieldOfView = 120f;
	public float maxSight = 10f;
	public Vector3 rayPlayerOffset;
	public GameObject patrol_nodes;
	public bool allowedToSeePlayer = true;
	public bool idleOnStart = false;
	public AudioClip[] footstep_audioclips;
	public AudioClip[] attack_audioclips;
	public AudioClip[] randomNoises;
	public AudioClip mainEnemyAudio;
	public AudioClip playerSeenAudio;

	private GameObject player;
	private Transform playerTransform;
	private NavMeshAgent nav;
	private Animator anim;

	private Waypoint[] waypoints;
	private Waypoint current_waypoint;
	private int current_waypoint_index;
	private float timer = 0f;
	public float wayPointThreshold = .1f;
	private bool goBackWaypointReached = false;
	private int incrementMultiplier = 1;

	private Vector3 lastPlayerPosition;
	private bool playerLocated;

	private float originalAudioMinDistance;
	private float originalAudioMaxDistance;
	private float originalNavSpeed;

	public enum EnemyState { Idle = 0, Patrolling, ChasingPlayer, Attacking }
	private EnemyState enemyState = EnemyState.Idle;


	/* On object start */
	void Awake()
	{
		// Find and instantiate references
		player = GameObject.FindGameObjectWithTag ("Player");
		playerTransform = player.transform;
		nav = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> ();

		// Calculate route
		if (patrol_nodes)
			LoadWaypoints (patrol_nodes);

		// Decide first enemy state
		if (idleOnStart) enemyState = EnemyState.Idle;
		enemyState = EnemyState.Patrolling;

		// Audio distances
		originalAudioMaxDistance = GetComponent<AudioSource>().maxDistance;
		originalAudioMinDistance = GetComponent<AudioSource>().minDistance;
	}

	/* Return player instance */
	public GameObject getPlayer() {
		return this.player;
	}

	/* See player */
	public void playerSeen(){

		// If enemy cannot see player, do not calculate anything
		if (!allowedToSeePlayer) return;

		// Set stopping distance
		nav.stoppingDistance = stoppingDistancePlayer;

		// Tell script player has been located
		lastPlayerPosition = playerTransform.position;

		// Update enemy state if enemy isn't already chasing player or attacking him
		if (enemyState != EnemyState.Attacking && enemyState != EnemyState.ChasingPlayer){
			enemyState = EnemyState.ChasingPlayer;
			GetComponent<AudioSource>().PlayOneShot (playerSeenAudio, 0.7f);
		}

		// Update enemy's destination with Player position
		nav.SetDestination (lastPlayerPosition);

		playerLocated = true;

		// Reset timer
		timer = 0f;

	}

	/* Load waypoints and it's properties
	 * to determine enemy's behaviour */
	public void LoadWaypoints(GameObject nodes){

		// Update information
		patrol_nodes = nodes;

		// Declare waypoints with information from patrol nodes
		waypoints = nodes.GetComponentsInChildren<Waypoint> ();

		// If there are no waypoints to be followed
		if (waypoints.Length == 0) {

			current_waypoint = null;
			current_waypoint_index = -1;

			return;
		}

		// Define current waypoint as first from list
		current_waypoint = waypoints [0];

		// First index
		current_waypoint_index = 0;
		incrementMultiplier = 1;

		// Reverse direction node not reached
		goBackWaypointReached = false;

		nav.SetDestination (current_waypoint.transform.position);
	}

	/* Cast a ray across enemy's line of sight to check if player
	 * has been seen. If yes, chase player */
	bool CalculatePlayerInSight(){
		//-- DEBUG
		Debug.DrawRay (transform.position, transform.forward * maxSight, Color.green);

		//if (GameInspector.isOnCutscene) return false;

		// If player is within view distance
		if (Vector3.Distance(this.transform.position, playerTransform.position) < maxSight)
		{	
			// Find enemy position and direction towards the player
			Vector3 enemyPos = transform.position + (Vector3.up * nav.height * 0.5f);
			Vector3 dir = ((playerTransform.position + rayPlayerOffset) - (enemyPos));
			
			// Find angle between enemy looking position and desired direction
			float angle = Vector3.Angle (transform.forward, dir);
			
			// If desired direction is within field of view
			if ( angle < fieldOfView * 0.5f)
			{
				// -- DEBUG
				Debug.DrawRay (enemyPos, dir, Color.blue);
				
				// Cast a ray to see if Player is hit
				RaycastHit hit;
				if (Physics.Raycast(enemyPos, dir.normalized, out hit, maxSight)){
					
					if (hit.collider.CompareTag ("Player")){
						// -- DEBUG
						Debug.DrawLine (enemyPos, hit.point, Color.red);

						playerSeen();
						return true;
					}
				}	
			}
		}

		return false;
	}

	/* Go through patrol nodes to set enemy's
	 * route using waypoints */
	void Patrol(){
		if (Vector3.Distance (nav.destination, transform.position) <= wayPointThreshold){
			
			// Stall seconds
			if (timer == 0f) timer = Time.time + current_waypoint.secondsToStall;
			
			// Stall time has finished
			if (timer < Time.time){
				
				// Reached last waypoint
				if (current_waypoint_index >= waypoints.Length ||
				    current_waypoint_index <= 0){
					
					// Do we have to assign path immediately?
					if (current_waypoint.assignNewPathImmediately){
						LoadWaypoints(current_waypoint.pathToAssign);
					}
					// Do we have to go backwards now?
					else if (current_waypoint.goBackOnThisNode){
						incrementMultiplier = -1;
						goBackWaypointReached = true;
						
						//Debug.LogWarning("goBackWaypointReached true!");
						
						// Is this the node that we assign a new path?
					} else if (current_waypoint.nodeAfterReturnAssign && goBackWaypointReached){
						LoadWaypoints(current_waypoint.pathToAssign);
						
						//Debug.LogWarning("nodeAfterReturnAssignReached true!");
					}
				} 
				
				// Add or subtract 1 from current waypoint index
				if (current_waypoint_index < waypoints.Length &&
				    current_waypoint_index >= 0) {
					
					// Set new destination
					current_waypoint = waypoints[current_waypoint_index];
					nav.SetDestination(current_waypoint.transform.position);
					
					
				}
				
				current_waypoint_index += 1 * incrementMultiplier;
				timer = 0f;
			}
		}
	}

	/* Main update method for enemy AI */
	void Update()
	{
		//Debug.Log (nav.velocity.sqrMagnitude);
		// Play main audio
		if (!GetComponent<AudioSource>().isPlaying) {
			GetComponent<AudioSource>().clip = mainEnemyAudio;
			GetComponent<AudioSource>().Play();
			GetComponent<AudioSource>().loop = true;
		}

		//Debug.Log (enemyState);
		// Assume speed based upon state
		if (enemyState == EnemyState.Idle) nav.speed = 0f;
		else if (enemyState == EnemyState.Patrolling) nav.speed = patrolSpeed;

		// Update animator speed every single time
		anim.SetFloat ("Speed", Mathf.Clamp (nav.velocity.sqrMagnitude, 0.0f, 9.0f));
		anim.SetFloat ("DistanceToPlayer", Mathf.Clamp (Vector3.Distance (transform.position, playerTransform.position) - stoppingDistancePlayer, 0.0f, 2.5f));

		// Stopping distance from anything but the player
		if (enemyState != EnemyState.ChasingPlayer && enemyState != EnemyState.Attacking) nav.stoppingDistance = stoppingDistanceOther;

		// Manual check against colliding with player
		if (Vector3.Distance (nav.nextPosition, playerTransform.position) < nav.stoppingDistance)
			nav.velocity = Vector3.zero; 

		// If enemy is allowed to see the player calculate sight
		if (allowedToSeePlayer)
			playerLocated = CalculatePlayerInSight ();
		// Done by coroutine now

		// If enemy should patrol
		if (enemyState == EnemyState.Patrolling && nav.destination != null && current_waypoint != null)
			Patrol();

		// If enemy is chasing player
		if (enemyState == EnemyState.ChasingPlayer) {

			// Calculate speed to use
			if (Vector3.Distance(transform.position, playerTransform.position) > 6.5f) 
				nav.speed = chaseRunSpeed;
			else 
				nav.speed = chaseWalkSpeed;

			// And it has lost its track
			if (!playerLocated || !allowedToSeePlayer){

				if (timer == 0.0f) timer = Time.time + secondsToWaitOnLost;
				if (timer < Time.time) {

					timer = 0.0f;
					enemyState = EnemyState.Patrolling;
				}
			}

			if (anim.GetFloat("DistanceToPlayer") < 2.5f && playerLocated){
				enemyState = EnemyState.Attacking;
				anim.SetBool("isAttacking", true);
			}

		}

		// If enemy is attacking
		if (enemyState == EnemyState.Attacking) {

			if (!playerLocated || !allowedToSeePlayer){
				anim.SetBool("isAttacking", false);
				enemyState = EnemyState.ChasingPlayer;
			}
		}
	}

	/* Play sound using enemy Audio Source. Called by animation */
	public void PlaySound(AudioClip clip, bool stretchRange){

		GetComponent<AudioSource>().loop = false;

		if (stretchRange) {
			originalAudioMaxDistance = GetComponent<AudioSource>().maxDistance;
			originalAudioMinDistance = GetComponent<AudioSource>().minDistance;

			GetComponent<AudioSource>().minDistance = 0.1f;
			GetComponent<AudioSource>().maxDistance = 200.0f;
			StartCoroutine (normalizeRange ());
		}

		GetComponent<AudioSource>().clip = clip;
		GetComponent<AudioSource>().Play ();


	}

	/* Play footstep audio */
	public void PlayFootstepAudio(){
		int fs_index = Random.Range (0, footstep_audioclips.Length);
		//audio.volume = 1f;
		GetComponent<AudioSource>().PlayOneShot(footstep_audioclips[fs_index]);
	}

	/* Play attack audio */
	public void PlayAttackAudio(){
		int as_index = Random.Range (0, attack_audioclips.Length);
		//audio.volume = 1f;
		GetComponent<AudioSource>().PlayOneShot(attack_audioclips[as_index]);
	}

	/* Start patrolling */
	public void StartPatrolling(){
		enemyState = EnemyState.Patrolling;
	}

	/* Make enemy inactive */
	public void makeInactive(){
		enemyState = EnemyState.Idle;
		allowedToSeePlayer = false;
		//StopAllCoroutines ();
		gameObject.SetActive (false);
		//StopCoroutine ("checkPlayer");

		Debug.Log ("Disabled!");
	}

	/* Make enemy active */
	public void makeActive(bool canSeePlayer, Vector3 position){
		enemyState = EnemyState.Idle;
		allowedToSeePlayer = canSeePlayer;
		transform.position = position;
		gameObject.SetActive (true);

	}

	/* Assign random node to enemy */
	public void assignRandomNode(float distance){
		if (waypoints == null) return;
		int random_index = Random.Range (0, waypoints.Length);
		if (Vector3.Distance(playerTransform.position, waypoints[random_index].transform.position) < distance) assignRandomNode(distance);
		current_waypoint = waypoints [random_index];
		current_waypoint_index = random_index;
		transform.position = current_waypoint.transform.position;
	}

	/* Coroutines */
	IEnumerator normalizeRange(){
		while(true){
			if (!GetComponent<AudioSource>().isPlaying){
				GetComponent<AudioSource>().minDistance = originalAudioMinDistance;
				GetComponent<AudioSource>().maxDistance = originalAudioMaxDistance;
			}
			yield return new WaitForSeconds(.1f);
		}
	}
}
