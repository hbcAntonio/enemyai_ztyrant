﻿using UnityEngine;
using System.Collections;

public class SeekPlayer : MonoBehaviour {

	private NavMeshAgent agent;
	private GameObject player;
	private Animator animator;

	// Use this for initialization
	void Start () {
		agent = this.GetComponent<NavMeshAgent> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		animator = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		agent.SetDestination (player.transform.position);
		animator.SetFloat("Speed", agent.velocity.sqrMagnitude);
	}
}
