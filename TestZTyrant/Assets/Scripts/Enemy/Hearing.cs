﻿using UnityEngine;
using System.Collections;

public class Hearing : MonoBehaviour {

	public float triggerSpeed;
	private EnemyAI enemyAI;
	private GameObject player;
	private CharacterController playerController;
	private float limitSpeed = 10.0f;

	// Use this for initialization
	void Start () {
		//playerInsideTrigger = false;
		enemyAI = transform.parent.GetComponent<EnemyAI> ();
		player = enemyAI.getPlayer ();
		playerController = player.GetComponent<CharacterController> ();
	}

	void OnTriggerStay(Collider other){
		// If player is inside inner hearing...
		if (other.CompareTag (player.tag)) {

			//... and player is moving 
			if (playerController.velocity.magnitude > triggerSpeed){
				//Debug.Log("Vel? " + playerController.velocity.magnitude);
				if (playerController.velocity.magnitude > limitSpeed) return;
				enemyAI.playerSeen();
			}
		}
	}
}
