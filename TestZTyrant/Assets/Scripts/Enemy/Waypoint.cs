﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour {
	public float secondsToStall = 0.0f;
	public GameObject pathToAssign;
	public bool goBackOnThisNode = false;
	public bool nodeAfterReturnAssign = false;
	public bool assignNewPathImmediately = false;

	void Awake(){
		if (assignNewPathImmediately){
			goBackOnThisNode = false;
			nodeAfterReturnAssign = false;
		}
	}
}
